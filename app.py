from flask import Flask, render_template, url_for, flash, redirect

from forms import RegistrationForm, LoginForm, CreateForm, UpdateForm, DeleteForm

import json
loginBoolean = False
property_id = 10
username=''

app = Flask(__name__)
app.config['SECRET_KEY'] = '7ba847da83ca3ef43a2c206b8c2c6bcc'

with open('posts.txt') as posts:
    posts=json.load(posts)


with open('user_list.txt') as user_list:
    user_list=json.load(user_list)


@app.route("/")
@app.route("/home")
def home():
    return render_template('home.html', posts=posts)


@app.route("/about")
def about():
    return render_template('about.html', title='About')


@app.route("/register", methods=['GET', 'POST'])
def register():
    form = RegistrationForm()
    user_list.update({form.username.data: {"password": form.password.data, "properties": [], "bookings": []}})
    with open('user_list.txt', 'w') as outfile:
        json.dump(user_list, outfile)
    if form.validate_on_submit():
        flash(f'Account created for {form.username.data}!', 'success')
        return redirect(url_for('home'))
    return render_template('register.html', title='Register', form=form)


@app.route("/login", methods=['GET', 'POST'])
def login():
    global loginBoolean
    global username
    form = LoginForm()
    if form.validate_on_submit():
        if form.password.data == user_list.get(form.username.data).get("password"):
            loginBoolean = True
            username =form.username.data
            flash('You have been logged in!', 'success')
            return redirect(url_for('home'))
        else:
            flash('Login Unsuccessful. Please check username and password', 'danger')
            return (loginBoolean) (redirect(url_for('login')))
    return render_template('login.html', title='Login', form=form)


@app.route("/create", methods=['GET', 'POST'])
def create():
    global loginBoolean
    global property_id
    if loginBoolean == False:
        flash(f'You have to be logged in','danger')
        return redirect(url_for('login'))
    else:
        form = CreateForm()
        if form.validate_on_submit():
            posts.append({'name': form.name.data, 'property_id': property_id, 'numOfRoom': form.numOfRoom.data,
                          'numOfBedroom': form.numOfBedroom.data, 'sqmeter': form.sqmeter.data})
            with open('posts.txt', 'w') as outfile:
                json.dump(posts, outfile)
            user_list.update({username: {'properties':[{'property_id': property_id, 'name': form.name.data,'numOfRoom': form.numOfRoom.data, 'numOfBedroom': form.numOfBedroom.data,'sqmeter': form.sqmeter.data}]}})
            with open('user_list.txt', 'w') as outfile:
                json.dump(user_list, outfile)
            property_id = property_id+1

            flash(f'New post has been created !', 'success')
            return redirect(url_for('home'))
    return render_template('create.html', title='Create', form=form)


@app.route('/update', methods=['GET', 'POST'])
def update():
    global loginBoolean
    global property_id
    if loginBoolean == False:
        flash(f'You have to be logged in', 'danger')
        return redirect(url_for('login'))
    else:
        form = UpdateForm()
        if form.validate_on_submit():
            posts.append({'name': form.name.data, 'property_id': property_id, 'numOfRoom': form.numOfRoom.data,
                          'numOfBedroom': form.numOfBedroom.data, 'sqmeter': form.sqmeter.data})
            with open('posts.txt', 'w') as outfile:
                json.dump(posts, outfile)
            user_list.update({username: {'properties':[{'property_id': property_id, 'name': form.name.data,'numOfRoom': form.numOfRoom.data, 'numOfBedroom': form.numOfBedroom.data,'sqmeter': form.sqmeter.data}]}})
            with open('user_list.txt', 'w') as outfile:
                json.dump(user_list, outfile)
            flash(f'Updated', 'success')
            return redirect(url_for('home'))
    return render_template('update.html', title='Update', form=form)


@app.route('/delete', methods=['GET','POST','DELETE'])
def delete():
    global loginBoolean
    global property_id
    if not loginBoolean:
        flash(f'You have to be logged in', 'danger')
        return redirect(url_for('login'))
    else:
        form = DeleteForm()
        if form.validate_on_submit():
            flash(f'Deleted', 'success')
            return redirect(url_for('home'))
    return render_template('delete.html', title='Delete', form=form)


@app.route('/logout', methods=['GET'])
def logout():
    global loginBoolean
    loginBoolean = False
    return render_template('logout.html', title='Logout')

if __name__ == '__main__':
    app.run(debug=True)
