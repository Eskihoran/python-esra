from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, SubmitField, BooleanField
from wtforms.validators import DataRequired, Length, EqualTo, NoneOf
import json

with open('user_list.txt') as user_list:
    user_list=json.load(user_list)

usernames = dict.keys((user_list))


class RegistrationForm(FlaskForm):
    username = StringField('Username',
                           validators=[DataRequired(), Length(min=2, max=20),NoneOf(usernames,'Choose another username!')])
    password = PasswordField('Password', validators=[DataRequired()])
    confirm_password = PasswordField('Confirm Password',
                                     validators=[DataRequired(), EqualTo('password')])
    submit = SubmitField('Sign Up')


class LoginForm(FlaskForm):
    username = StringField('Username',validators=[DataRequired()])
    password = PasswordField('Password', validators=[DataRequired()])
    remember = BooleanField('Remember Me')
    submit = SubmitField('Login')


class CreateForm(FlaskForm):
    name = StringField('Name Of Post',validators=[DataRequired()])
    numOfRoom = StringField('Number of Rooms', validators=[DataRequired()])
    numOfBedroom = StringField('Number of Bedrooms', validators=[DataRequired()])
    sqmeter = StringField('Sq Meter of House', validators=[DataRequired()])
    submit = SubmitField('Create')


class UpdateForm(FlaskForm):
    property_id = StringField('Property ID', validators=[DataRequired()])
    name = StringField('Name Of Post',validators=[DataRequired()])
    numOfRoom = StringField('Number of Rooms', validators=[DataRequired()])
    numOfBedroom = StringField('Number of Bedrooms', validators=[DataRequired()])
    sqmeter = StringField('Sq Meter of House', validators=[DataRequired()])
    submit = SubmitField('Update')


class DeleteForm(FlaskForm):
    property_id = StringField('Property ID', validators=[DataRequired()])
    submit = SubmitField('Delete')



